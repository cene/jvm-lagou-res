import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class UnsafeDemo {
    public static final int _1MB = 1024 * 1024;

    public static void main(String[] args) throws Exception {
        Field field = Unsafe.class.getDeclaredField("theUnsafe");
        field.setAccessible(true);
        Unsafe unsafe = (Unsafe) field.get(null);
        for (; ; ) {
            long addr = unsafe.allocateMemory(_1MB);
        }
    }
}
