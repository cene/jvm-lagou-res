public class WaitDemo {
    public static void main(String[] args) throws Exception {
        Object o = new Object();
        new Thread(() -> {
            try {
                synchronized (o) {
                    o.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "wait-demo").start();

        Thread.sleep(1000);

        synchronized (o) {
            o.wait();
        }
    }
}
